package com.batch2.latihan.model;

import javax.persistence.FetchType;
import jdk.jfr.Enabled;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_matakuliah")
@NoArgsConstructor
public class Matakuliah {
    @Id
    @Column(name = "kode_mk")
    private Long kodeMk;
    private String namaMk;
    private String sks;

    @OneToMany(mappedBy = "matakuliah")
    private List<Mahasiswa> mahasiswa;

    public Matakuliah(Long kodeMk){
        this.kodeMk = kodeMk;
    }
}
