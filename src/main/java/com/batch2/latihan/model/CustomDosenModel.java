package com.batch2.latihan.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNativeJoin", entities = {
        @EntityResult( entityClass = CustomDosenModel.class, fields = {
                @FieldResult(name = "nip", column = "nip"),
                @FieldResult(name = "namaDosen", column = "nama_dosen"),
                @FieldResult(name = "namaMatkul", column = "nama_mk"),
                @FieldResult(name = "nim", column = "nim"),
                @FieldResult(name = "namaMahasiswa", column = "nama_mhs"),
        })
})
public class CustomDosenModel {
    @Id
    private Long nip;
    private String namaDosen;
    private String namaMatkul;
    private String nim;
    private String namaMahasiswa;
}