package com.batch2.latihan.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_dosen")
public class Dosen {
    @Id
    private Long nip;
    private String namaDosen;

    @OneToOne
    @JoinColumn(name = "kode_matkul")
    private Matakuliah matakuliah;

}

