package com.batch2.latihan.model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_mahasiswa")
public class Mahasiswa {
    @Id
    private Long nim;
    private String namaMhs;
    private String alamat;

    @ManyToOne
    @JoinColumn(name = "kode_matkul")
    @JsonBackReference
    private Matakuliah matakuliah;
}