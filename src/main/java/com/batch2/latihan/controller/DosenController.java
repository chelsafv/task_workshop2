package com.batch2.latihan.controller;

import com.batch2.latihan.dto.DosenDto;
import com.batch2.latihan.model.*;
import com.batch2.latihan.repository.DosenRepository;
import com.batch2.latihan.repository.MahasiswaRepository;
import com.batch2.latihan.repository.MatakuliahRepository;
import com.batch2.latihan.services.CustomQueryDAO;
import com.batch2.latihan.services.DosenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping(path = "/myDosen")
public class DosenController {

    @Autowired
    private DosenService dosenService;

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MatakuliahRepository matakuliahRepository;

    @Autowired
    private CustomQueryDAO ccDao;

    @GetMapping("/getAllDosen")
    public ResponseEntity<List<Dosen>> getAllUser(){
        List<Dosen> result = dosenRepository.findAll();
        if (result == null || result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/saveDosen")
    public  ResponseEntity<Dosen> saveUser(@RequestBody Dosen dosen){
        try{
            Dosen u = dosenRepository.save(dosen);
            return new ResponseEntity<>(u, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error saving {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/saveMhs")
    public  ResponseEntity<Dosen> saveUser(@RequestBody Mahasiswa mhs){
        try{
            Mahasiswa u = mahasiswaRepository.save(mhs);
            return new ResponseEntity(u, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error saving {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/saveMatkul")
    public  ResponseEntity<Dosen> saveUser(@RequestBody Matakuliah matkul){
        try{
            Matakuliah u = matakuliahRepository.save(matkul);
            return new ResponseEntity(u, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error saving {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/Query")
    public ResponseEntity<List<CustomDosenModel>> getQuery(@RequestParam Long nip){
        List<CustomDosenModel> list = ccDao.getQueryNative(nip);
        return ResponseEntity.ok(list);
    }
}
