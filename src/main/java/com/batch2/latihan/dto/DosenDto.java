package com.batch2.latihan.dto;
import lombok.Data;

@Data
public class DosenDto {
    private Long nip;
    private String namaDosen;
}
