package com.batch2.latihan.repository;
import com.batch2.latihan.model.Dosen;
import com.batch2.latihan.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DosenRepository extends JpaRepository<Dosen, Long> {
}
