package com.batch2.latihan.services;

import com.batch2.latihan.model.CustomDosenModel;
import com.batch2.latihan.model.CustomMappingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQueryDAO {

    @Autowired
    private EntityManager em;

    public List<CustomMappingModel> getCustomQueryNative(String userName){
        String nativeQueryScript = "select RAND(100) AS id, mu.username, mr.role_name, mp.url from mst_user mu \n" +
                "inner join mst_role mr on mr.id = mu.role_id \n" +
                "inner join mst_permissions mp on mp.role_id = mr.id where mu.username = :userName \n";
        Query q = em.createNativeQuery(nativeQueryScript, "QueryNativePakeJoin");

        List<CustomMappingModel> list = q.setParameter("userName",userName).getResultList();

        return list;
    }
    public List<CustomDosenModel> getQueryNative(Long nip){
        String nativeQueryScript = "select td.nip, td.nama_dosen, tm.nama_mk, ts.nim, ts.nama_mhs from tbl_dosen td \n" +
                "inner join tbl_matakuliah tm on tm.kode_mk = td.kode_matkul \n" +
                "inner join tbl_mahasiswa ts on ts.kode_matkul = tm.kode_mk where td.nip = :nip \n";
        Query y = em.createNativeQuery(nativeQueryScript, "QueryNativeJoin");

        List<CustomDosenModel> list = y.setParameter("nip",nip).getResultList();

        return list;
    }
}
