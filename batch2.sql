-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2021 at 09:59 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `batch2`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_mapping_model`
--

CREATE TABLE `custom_mapping_model` (
  `id` varchar(255) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mst_permissions`
--

CREATE TABLE `mst_permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mst_permissions`
--

INSERT INTO `mst_permissions` (`id`, `role_id`, `url`) VALUES
(1, 1, '/admin/config'),
(2, 1, '/admin/userManagement'),
(3, 2, '/cust/cust_registration'),
(4, 2, '/cust/inquiries'),
(5, 3, '/trx/b2b'),
(6, 3, '/trx/withdrawal'),
(7, 3, '/trx/deposit'),
(8, 3, '/trx/loan'),
(9, 2, '/cust/bypasstoken');

-- --------------------------------------------------------

--
-- Table structure for table `mst_role`
--

CREATE TABLE `mst_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(40) DEFAULT NULL,
  `permission_desc` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mst_role`
--

INSERT INTO `mst_role` (`id`, `role_name`, `permission_desc`) VALUES
(1, 'Admin', 'Role Administrasi'),
(2, 'CS', 'Role Customer Service'),
(3, 'TL', 'Role Teller Bank');

-- --------------------------------------------------------

--
-- Table structure for table `mst_user`
--

CREATE TABLE `mst_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mst_user`
--

INSERT INTO `mst_user` (`id`, `username`, `email`, `role_id`) VALUES
(1, 'Triss', 'triss@mail.com', 2),
(2, 'Marrie', 'mr@mail.com', 2),
(3, 'Jasmine', 'ys@mail.com', 3),
(4, 'Rajit', 'rj@mail.com', 1),
(5, 'Marrie', 'mr@mail.com', 1),
(6, 'Khansa', 'kh@mail.com', 2),
(7, 'Chelsa', 'ch@mail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` varchar(35) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `image` varchar(3000) CHARACTER SET latin1 NOT NULL,
  `kodeproduct` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name_product` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 DEFAULT NULL,
  `price` decimal(20,0) NOT NULL,
  `discount` decimal(3,0) NOT NULL,
  `cost` decimal(11,0) DEFAULT NULL,
  `fee_outlet` decimal(11,0) NOT NULL,
  `check_code` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `pay_code` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `menu_id` varchar(15) CHARACTER SET latin1 NOT NULL,
  `product_group_id` varchar(35) CHARACTER SET latin1 DEFAULT NULL,
  `product_sales_id` varchar(35) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `image`, `kodeproduct`, `name_product`, `description`, `price`, `discount`, `cost`, `fee_outlet`, `check_code`, `pay_code`, `menu_id`, `product_group_id`, `product_sales_id`, `created_at`, `updated_at`, `is_deleted`) VALUES
('1234', '-', '1234', 'Pulsa Indosat', 'Desk Pulsa Indosat', '100000', '10', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-10-12 10:58:00', '2020-10-12 10:58:00', 1),
('BK001', 'imageNyaYa', 'BPJSKS', 'BPJS KESEHATAN', 'BPJS KESEHATAN', '0', '0', '2500', '0', 'HBPJSKSCEK', 'HBPJSKSBYR', 'MN011', 'BPJS', NULL, '2019-12-29 14:02:04', '2019-12-29 14:02:04', 0),
('BK002', 'imageNyaYa', 'BPJSTK', 'BPJS TENAGA KERJA', 'BPJS TENAGA KERJA', '0', '0', '2500', '0', 'HBPJSKSCEK', 'HBPJSKSBYR', 'MN011', 'BPJS', NULL, '2020-01-06 21:44:10', '2020-01-06 21:44:10', 0),
('CK001', 'imageNyaYa', 'BAF', 'BAF', 'BAF', '0', '0', '7000', '0', 'HBAFCEK', 'HBAFBYR', 'MN007', NULL, NULL, '2019-12-29 13:46:15', '2019-12-29 13:46:15', 0),
('CK002', 'imageNyaYa', 'FIF', 'FIF', 'FIF', '0', '0', '5000', '0', 'HFIFCEK', 'HFIFBYR', 'MN007', NULL, NULL, '2019-12-29 13:46:15', '2019-12-29 13:46:15', 0),
('CK003', 'imageNyaYa', 'MEGA CENTRAL FINANCE ( MCF )', 'MEGA CENTRAL FINANCE ( MCF )', 'MEGA CENTRAL FINANCE ( MCF )', '0', '0', '2000', '0', 'HMCFCEK', 'HMCFBYR', 'MN007', NULL, NULL, '2019-12-29 13:46:15', '2019-12-29 13:46:15', 0),
('CK004', 'imageNyaYa', 'WOM FINANCE', 'WOM FINANCE', 'WOM FINANCE', '0', '0', '0', '0', 'HWOMCEK', 'HWOMBYR', 'MN007', NULL, NULL, '2019-12-29 13:46:15', '2019-12-29 13:46:15', 0),
('CK005', 'imageNyaYa', 'MPM', 'MPM', 'MPM', '0', '0', '0', '0', 'HMPMCEK', 'HMPMBYR', 'MN007', NULL, NULL, '2019-12-29 13:46:15', '2019-12-29 13:46:15', 0),
('INDOSAT001', '-', 'IH5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2019-12-29 17:19:21', '2019-12-29 17:19:21', 0),
('INDOSAT002', '-', 'IH10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2019-12-29 17:19:21', '2019-12-29 17:19:21', 0),
('INDOSAT003', '-', 'IH20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:17', '2020-01-11 08:21:17', 0),
('INDOSAT004', '-', 'IH25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:17', '2020-01-11 08:21:17', 0),
('INDOSAT005', '-', 'IH30', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:17', '2020-01-11 08:21:17', 0),
('INDOSAT006', '-', 'IH50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:17', '2020-01-11 08:21:17', 0),
('INDOSAT007', '-', 'IH100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:17', '2020-01-11 08:21:17', 0),
('INDOSAT008', '-', 'IP5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:29', '2020-01-11 08:21:29', 0),
('INDOSAT009', '-', 'IP10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:29', '2020-01-11 08:21:29', 0),
('INDOSAT010', '-', 'IP25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:29', '2020-01-11 08:21:29', 0),
('INDOSAT011', '-', 'IP50', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:29', '2020-01-11 08:21:29', 0),
('INDOSAT012', '-', 'IP100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:29', '2020-01-11 08:21:29', 0),
('INDOSAT013', '-', 'MB5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:38', '2020-01-11 08:21:38', 0),
('INDOSAT014', '-', 'MB10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'INDOSAT_PRABAYAR', NULL, '2020-01-11 08:21:38', '2020-01-11 08:21:38', 0),
('PDAM0001', '-', 'BOJO', 'PDAM BOJONEGORO', '-', '0', '0', '2500', '1125', 'HPBJGCEK', 'HPBJGBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0002', '-', 'BANGKA', 'PDAM BANGKALAN', '-', '0', '0', '2500', '900', 'HPBKLCEK', 'HPBKLBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0003', '-', 'BLIT', 'PDAM BLITAR', '-', '0', '0', '2500', '975', 'HPBLICEK', 'HPBLIBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0004', '-', 'BANYU', 'PDAM BANYUWANGI', '-', '0', '0', '2500', '700', 'HPBWICEK', 'HPBWIBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0005', '-', 'BONDO', 'PDAM BONDOWOSO', '-', '0', '0', '2000', '500', 'HPBWOCEK', 'HPBWOBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0006', '-', 'GRES', 'PDAM GRESIK', '-', '0', '0', '2000', '750', 'HPGSKCEK', 'HPGSKBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0007', '-', 'JOMB', 'PDAM JOMBANG', '-', '0', '0', '2000', '750', 'HPJBGCEK', 'HPJBGBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0008', '-', 'JEM', 'PDAM JEMBER', '-', '0', '0', '0', '450', 'HPJBRCEK', 'HPJBRBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0009', '-', 'KEDI', 'PDAM KEDIRI', '-', '0', '0', '2000', '950', 'HPKDRCEK', 'HPKDRBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0010', '-', 'BLIT', 'PDAM KOTA BLITAR', '-', '0', '0', '2500', '900', 'HPKTBLICEK', 'HPKTBLIBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0011', '-', 'KOKEDI', 'PDAM KOTA KEDIRI', '-', '0', '0', '2500', '950', 'HPKTKDRCEK', 'HPKTKDRBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0012', '-', 'LAMO', 'PDAM LAMONGAN', '-', '0', '0', '2500', '750', 'HPLMGCEK', 'HPLMGBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0013', '-', 'LUMA', 'PDAM LUMAJANG', '-', '0', '0', '2300', '750', 'HPLMJCEK', 'HPLMJBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0014', '-', 'MAGE', 'PDAM MAGETAN', '-', '0', '0', '2000', '750', 'HPMGTCEK', 'HPMGTBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0015', '-', 'MOJO', 'PDAM MOJOKERTO', '-', '0', '0', '2000', '750', 'HPMJKCEK', 'HPMJKBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0016', '-', 'KABMAL', 'PDAM KAB. MALANG', '-', '0', '0', '2000', '950', 'HPMLGKBCEK', 'HPMLGKBBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0017', '-', 'H2H BYR - PDAM KOTA MALANG', 'PDAM KOTA MALANG', '-', '0', '0', '2500', '975', 'HPMLGKTCEK', 'HPMLGKTBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0018', '-', 'NGANJ', 'PDAM NGANJUK', '-', '0', '0', '2500', '750', 'HPNJKCEK', 'HPNJKBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0019', '-', 'PROBOL', 'PDAM PROBOLINGGO', '-', '0', '0', '2500', '463', 'HPPBLCEK', 'HPPBLBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0020', '-', 'KOTPROBOL', 'PDAM KOTA PROBOLINGGO', '-', '0', '0', '2500', '463', 'HPPBLKTCEK', 'HPPBLKTBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0021', '-', 'PONO', 'PDAM PONOROGO', '-', '0', '0', '2000', '675', 'HPPONCEK', 'HPPONBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0022', '-', 'PASU', 'PDAM PASURUAN', '-', '0', '0', '2500', '750', 'HPPSRCEK', 'HPPSRBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0023', '-', 'SURAB', 'PDAM SURABAYA', '-', '0', '0', '2000', '650', 'HPSBYCEK', 'HPSBYBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0024', '-', 'SIDO', 'PDAM SIDOARJO', '-', '0', '0', '1800', '650', 'HPSDACEK', 'HPSDABYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0025', '-', 'SITU', 'PDAM SITUBONDO', '-', '0', '0', '2000', '650', 'HPSTBCEK', 'HPSTBBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0026', '-', 'TUBA', 'PDAM TUBAN', '-', '0', '0', '2000', '750', 'HPTBNCEK', 'HPTBNBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0027', '-', 'TULUNG', 'PDAM TULUNGAGUNG', '-', '0', '0', '2500', '975', 'HPTLGCEK', 'HPTLGBYR', 'MN003', 'PDAM001', NULL, '2019-12-29 16:15:04', '2019-12-29 16:15:04', 0),
('PDAM0028', '-', 'DENPA', 'PDAM DENPASAR', '-', '0', '0', '2500', '500', 'HPDPSCEK', 'HPDPSBYR', 'MN003', 'PDAM002', NULL, '2019-12-29 16:23:24', '2019-12-29 16:23:24', 0),
('PDAM0029', '-', 'BOYO', 'PDAM KAB. BOYOLALI', '-', '0', '0', '2000', '850', 'HPBYLCEK', 'HPBYLBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0030', '-', 'KABKAR', 'PDAM KAB. KARANGANYAR', '-', '0', '0', '2000', '825', 'HPKBKRYCEK', 'HPKBKRYBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0031', '-', 'KLATEN', 'PDAM KLATEN', '-', '0', '0', '2000', '800', 'HPKLNCEK', 'HPKLNBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0032', '-', 'PEKA', 'PDAM KOTA PEKALONGAN', '-', '0', '0', '2500', '825', 'HPKTPKLCEK', 'HPKTPKLBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0033', '-', 'PDAMMDN', 'PDAM MAGELANG', '-', '0', '0', '2500', '725', 'HPMGLCEK', 'HPMGLBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0034', '-', 'PURBA', 'PDAM PURBALINGGA', '-', '0', '0', '2500', '825', 'HPPBGCEK', 'HPPBGBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0035', '-', ' PURWO ', 'PDAM PURWOREJO', '-', '0', '0', '0', '1250', 'HPPWJCEK', 'HPPWJBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0036', '-', 'SOLO', 'PDAM SOLO', '-', '0', '0', '2000', '800', 'HPSLOCEK', 'HPSLOBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0037', '-', 'WONO', 'PDAM WONOSOBO', '-', '0', '0', '2500', '500', 'HPWSBCEK', 'HPWSBBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0038', '-', 'KABBOY', 'PDAM Kab. Boyolali', '-', '0', '0', '2000', '850', 'HPBYLCEK', 'HPBYLBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0039', '-', 'KEBU', 'PDAM KAB. KEBUMEN', '-', '0', '0', '3000', '1050', 'HPKBKBMCEK', 'HPKBKBMBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0040', '-', 'JEPAR', 'PDAM KAB. JEPARA', '-', '0', '0', '3000', '850', 'HPKBJPRCEK', 'HPKBJPRBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0041', '-', 'SEMAR', 'PDAM SEMARANG', '-', '0', '0', '2500', '850', 'HPSMGCEK', 'HPSMGBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0042', '-', 'KABSUKO', 'PDAM KAB. SUKOHARJO', '-', '0', '0', '2000', '1000', 'HPKBSKJCEK', 'HPKBSKJBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0043', '-', 'PATI', 'PDAM PATI', '-', '0', '0', '500', '350', 'HPPTICEK', 'HPPTIBYR', 'MN003', 'PDAM003', NULL, '2019-12-29 16:36:29', '2019-12-29 16:36:29', 0),
('PDAM0044', '-', 'MTR', 'PDAM KOTA MATARAM', '-', '0', '0', '2000', '850', 'HPBYLCEK', 'HPBYLBYR', 'MN003', 'PDAM003', NULL, '2020-01-07 16:25:29', '2020-01-07 16:25:29', 0),
('PLN001', 'imageNyaYa', 'PLNPAS', 'PLN PASCA BAYAR', 'PLN PASCA BAYAR', '0', '0', '0', '0', 'HPLNCEK', 'HPLNBYR', 'MN004', 'PLN_PASCA_BAYAR', NULL, '2019-12-29 13:12:29', '2019-12-29 13:12:29', 0),
('PLN002', 'imageNyaYa', 'PLN TOKEN', 'PLN TOKEN', 'PLN TOKEN', '0', '0', '0', '0', 'IPCCEK', NULL, 'MN004', 'PLN_TOKEN', NULL, '2019-12-29 13:12:29', '2019-12-29 13:12:29', 0),
('PLN003', 'imageNyaYa', 'PLNON', 'PLN NONTAGLIS', 'PLN NONTAGLIS', '0', '0', '2500', '0', 'IPCCEK', NULL, 'MN004', 'PLN_NON_TAGLIS', NULL, '2020-01-16 17:52:54', '2020-01-16 17:52:54', 0),
('PR001', 'o90iyeftdr', 'TU5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', 'PROMO', '2019-12-16 10:49:23', '2019-12-16 10:49:23', 0),
('PR002', 'o90iyeftdr', 'TU10', '10000', 'Pulsa 10000', '10', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', 'PROMO', '2019-12-16 16:00:30', '2019-12-16 16:00:30', 0),
('PRTG001', 'imageNyaYa', 'PERTAGAS', 'PERTAGAS', 'PERTAGAS', '0', '0', '2500', '0', 'HPGASCEK', 'HPGASBYR', 'MN005', NULL, NULL, '2019-12-29 14:12:23', '2019-12-29 14:12:23', 0),
('PRTG002', 'imageNyaYa', 'PERTAGAS TOKEN', 'PERTAGAS TOKEN', 'PERTAGAS TOKEN', '0', '0', '2500', '0', 'HTGASCEK', NULL, 'MN005', NULL, NULL, '2019-12-29 14:12:23', '2019-12-29 14:12:23', 0),
('PRTG003', 'imageNyaYa', 'PGN', 'PGN', 'PGN', '0', '0', '2500', '0', 'HPGNCEK', 'HPGNBYR', 'MN005', NULL, NULL, '2019-12-29 14:12:23', '2019-12-29 14:12:23', 0),
('SMARTFREEN001', '-', 'SMH5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2019-12-29 17:16:50', '2019-12-29 17:16:50', 0),
('SMARTFREEN002', '-', 'SMH10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2019-12-29 17:16:50', '2019-12-29 17:16:50', 0),
('SMARTFREEN003', '-', 'SMH20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN004', '-', 'SMH25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN005', '-', 'SMH30', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN006', '-', 'SMH50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN007', '-', 'SMH60', '60000', 'Pulsa 60000', '61000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN008', '-', 'SMH100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN009', '-', 'SMH150', '150000', 'Pulsa 150000', '151000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN010', '-', 'SMH200', '200000', 'Pulsa 200000', '201000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN011', '-', 'SMH300', '300000', 'Pulsa 300000', '301000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN012', '-', 'SMH500', '500000', 'Pulsa 500000', '501000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', NULL, '2020-01-11 08:29:45', '2020-01-11 08:29:45', 0),
('SMARTFREEN013', '-', 'SMP5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN014', '-', 'SMP10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN015', '-', 'SMP20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN016', '-', 'SMP25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN017', '-', 'SMP30', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN018', '-', 'SMP50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN019', '-', 'SMP60', '60000', 'Pulsa 60000', '61000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('SMARTFREEN020', '-', 'SMP100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'SMARTFREN_PRABAYAR', 'PROMO', '2020-01-11 08:33:25', '2020-01-11 08:33:25', 0),
('TB001', 'imageNyaYa', 'INDOVISION', 'INDOVISION', 'INDOVISION', '0', '0', '2000', '0', 'HIDVSCEK', 'HIDVSBYR', 'MN010', NULL, NULL, '2019-12-29 13:54:08', '2019-12-29 13:54:08', 0),
('TB002', 'imageNyaYa', 'OKE TV', 'OKE TV', 'OKE TV', '0', '0', '2000', '0', 'HOKETVCEK', 'HOKETVBYR', 'MN010', NULL, NULL, '2019-12-29 13:54:08', '2019-12-29 13:54:08', 0),
('TB003', 'imageNyaYa', 'TOP TV', 'TOP TV', 'TOP TV', '0', '0', '2000', '0', 'HTOPCEK', 'HTOPTVBYR', 'MN010', NULL, NULL, '2019-12-29 13:54:08', '2019-12-29 13:54:08', 0),
('TB004', 'imageNyaYa', 'MNC PLAYMEDIA', 'MNC PLAYMEDIA', 'MNC PLAYMEDIA', '0', '0', '0', '0', 'HMNCPCEK', 'HMNCPBYR', 'MN010', NULL, NULL, '2019-12-29 13:54:08', '2019-12-29 13:54:08', 0),
('TB005', 'imageNyaYa', 'TRANSVISION', 'TRANSVISION', 'TRANSVISION', '0', '0', '0', '0', NULL, NULL, 'MN010', NULL, NULL, '2019-12-29 13:54:08', '2019-12-29 13:54:08', 0),
('TEL001', 'imageNyaYa', 'TELKOM', 'TELKOM', 'TELKOM', '0', '0', '0', '0', 'HTELCEK', 'HTELBYR', 'MN008', NULL, NULL, '2019-12-29 13:18:11', '2019-12-29 13:18:11', 0),
('TEL002', 'imageNyaYa', 'SPEEDY', 'SPEEDY', 'SPEEDY', '0', '0', '0', '0', 'HSPDCEK', 'HSPDBYR', 'MN008', NULL, NULL, '2019-12-29 13:18:11', '2019-12-29 13:18:11', 0),
('TELKOMSEL001', '-', 'MD5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:28:20', '2020-01-11 09:28:20', 0),
('TELKOMSEL002', '-', 'MD10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:28:20', '2020-01-11 09:28:20', 0),
('TELKOMSEL003', '-', 'MD20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:28:20', '2020-01-11 09:28:20', 0),
('TELKOMSEL004', '-', 'MD25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:28:20', '2020-01-11 09:28:20', 0),
('TELKOMSEL005', '-', 'MD50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:28:20', '2020-01-11 09:28:20', 0),
('TELKOMSEL006', '-', 'MD100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:28:20', '2020-01-11 09:28:20', 0),
('TELKOMSEL007', '-', 'TH5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL008', '-', 'TH10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL009', '-', 'TH15', '15000', 'Pulsa 15000', '15000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL010', '-', 'TH20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL011', '-', 'TH25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL012', '-', 'TH30', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL013', '-', 'TH40', '40000', 'Pulsa 40000', '41000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL014', '-', 'TH50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL015', '-', 'TH75', '75000', 'Pulsa 75000', '76000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL016', '-', 'TH100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:31:49', '2020-01-11 09:31:49', 0),
('TELKOMSEL017', '-', 'TP5', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-11 09:32:30', '2020-01-11 09:32:30', 0),
('TELKOMSEL018', '-', 'TP10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', 'PROMO', '2020-01-11 09:32:30', '2020-01-11 09:32:30', 0),
('TELKOMSEL021', '-', 'TU20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', 'PROMO', '2020-01-11 09:33:52', '2020-01-11 09:33:52', 0),
('TELKOMSEL022', '-', 'TU25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'TELKOMSEL_PRABAYAR', 'PROMO', '2020-01-11 09:33:52', '2020-01-11 09:33:52', 0),
('TELKOMSELDATA001', '-', 'TD001', '5000', '-', '6000', '0', '2000', '850', 'HPBYLCEK', 'HPBYLBYR', 'MN002', 'TELKOMSEL_PRABAYAR', NULL, '2020-01-10 22:49:09', '2020-01-10 22:49:09', 0),
('THREE001', '-', 'TRH1', '2000', 'Pulsa 2000', '3000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE002', '-', 'TRH2', '3000', 'Pulsa 3000', '4000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE003', '-', 'TRH3', '4000', 'Pulsa 4000', '5000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE004', '-', 'TRH4', '5000', 'Pulsa 5000', '6000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE005', '-', 'TRH5', '6000', 'Pulsa 6000', '7000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE006', '-', 'TRH10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE007', '-', 'TRH15', '15000', 'Pulsa 15000', '16000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE008', '-', 'TRH20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE009', '-', 'TRH25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE010', '-', 'TRH30', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE011', '-', 'TRH50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE012', '-', 'TRH100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', NULL, '2020-01-11 09:18:32', '2020-01-11 09:18:32', 0),
('THREE013', '-', 'TRP5', '6000', 'Pulsa 6000', '7000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE014', '-', 'TRP10', '10000', 'Pulsa 10000', '11000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE015', '-', 'TRP15', '15000', 'Pulsa 15000', '16000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE016', '-', 'TRP20', '20000', 'Pulsa 20000', '21000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE017', '-', 'TRP25', '25000', 'Pulsa 25000', '26000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE018', '-', 'TRP30', '30000', 'Pulsa 30000', '31000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE019', '-', 'TRP50', '50000', 'Pulsa 50000', '51000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0),
('THREE020', '-', 'TRP100', '100000', 'Pulsa 100000', '101000', '0', '0', '0', NULL, NULL, 'MN001', 'THREE_PRABAYAR', 'PROMO', '2020-01-11 09:21:13', '2020-01-11 09:21:13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_group`
--

CREATE TABLE `product_group` (
  `id` varchar(35) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name_group` varchar(55) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_group`
--

INSERT INTO `product_group` (`id`, `name_group`, `description`) VALUES
('AXIS_PRABAYAR', 'AXIS_PRABAYAR', 'AXIS_PRABAYAR'),
('BPJS', 'BPJS', 'Group Product BPJS'),
('INDIHOME', 'INDIHOME', 'Group Product Indihome'),
('INDOSAT_PRABAYAR', 'INDOSAT_PRABAYAR', 'INDOSAT_PRABAYAR'),
('PDAM001', 'PDAM JAWA TIMUR', 'Group Product PDAM JAWA TIMUR'),
('PDAM002', 'PDAM BALI', 'Group Product PDAM BALI'),
('PDAM003', 'PDAM JAWA TENGAH', 'Group Product PDAM JAWA TENGAH'),
('PG001', 'PROMO', 'Group Product Promo'),
('PG002', 'REQULER', 'Group Product Reguler'),
('PG006', 'Telpon', 'Group Product Telpon'),
('PG007', 'Indihome', 'Group Product Indihome'),
('PLN_NON_TAGLIS', 'PLN NON TAGLIS', 'Group Product Non Taglis'),
('PLN_PASCA_BAYAR', 'PLN PASCA BAYAR', 'Group Product PLN Pasca Bayar'),
('PLN_TOKEN', 'PLN TOKEN', 'Group Product PLN Token'),
('SMARTFREN_PRABAYAR', 'SMARTFREN_PRABAYAR', 'SMARTFREN_PRABAYAR'),
('TELKOMSEL_PRABAYAR', 'TELKOMSEL_PRABAYAR', 'TELKOMSEL_PRABAYAR'),
('TELPON', 'TELPON', 'Group Product Telpon'),
('THREE_PRABAYAR', 'THREE_PRABAYAR', 'THREE_PRABAYAR'),
('XL_PRABAYAR', 'XL_PRABAYAR', 'XL_PRABAYAR');

-- --------------------------------------------------------

--
-- Table structure for table `product_sales`
--

CREATE TABLE `product_sales` (
  `id` varchar(15) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name_group` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_sales`
--

INSERT INTO `product_sales` (`id`, `name_group`, `description`) VALUES
('PROMO', 'PROMO', 'PROMO'),
('REGULER', 'REGULER', 'REGULER');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dosen`
--

CREATE TABLE `tbl_dosen` (
  `nip` int(11) NOT NULL,
  `nama_dosen` varchar(200) DEFAULT NULL,
  `kode_matkul` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_dosen`
--

INSERT INTO `tbl_dosen` (`nip`, `nama_dosen`, `kode_matkul`) VALUES
(111, 'Budi M', 11),
(121, 'Eka Widya', 77),
(231, 'Muhammad', 87),
(987, 'Chelsa Farah', 99);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mahasiswa`
--

CREATE TABLE `tbl_mahasiswa` (
  `nim` int(11) NOT NULL,
  `nama_mhs` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `kode_matkul` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_mahasiswa`
--

INSERT INTO `tbl_mahasiswa` (`nim`, `nama_mhs`, `alamat`, `kode_matkul`) VALUES
(1, 'Chelsa', 'Malang', 11),
(2, 'Rio', 'Surabaya', 87),
(3, 'Adi', 'Malang', 87),
(4, 'Doni S', 'Surabaya', 11),
(5, 'Jeny', 'Jogja', 77),
(6, 'Hyuna', 'Solo', 77),
(1234, 'Virkhansa', 'Karanganyar', 99);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_matakuliah`
--

CREATE TABLE `tbl_matakuliah` (
  `kode_mk` int(11) NOT NULL,
  `nama_mk` varchar(200) DEFAULT NULL,
  `sks` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_matakuliah`
--

INSERT INTO `tbl_matakuliah` (`kode_mk`, `nama_mk`, `sks`) VALUES
(11, 'Kewarganegaraan', 2),
(65, 'Pemrograman', 3),
(73, 'Inggris', 2),
(77, 'Statistika', 2),
(87, 'Algoritma Dasar', 4),
(99, 'Bahasa Indoenesia', 3),
(401, 'Jaringan', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `custom_mapping_model`
--
ALTER TABLE `custom_mapping_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_permissions`
--
ALTER TABLE `mst_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKdt7nrv7thaaiul04xg3o883k3` (`role_id`);

--
-- Indexes for table `mst_role`
--
ALTER TABLE `mst_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_user`
--
ALTER TABLE `mst_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKobfmimbx942u4stre3a84a6av` (`role_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_ibfk_1` (`product_group_id`),
  ADD KEY `product_ibfk_3` (`product_sales_id`);

--
-- Indexes for table `product_group`
--
ALTER TABLE `product_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `K_product_group_id` (`id`) USING BTREE;

--
-- Indexes for table `product_sales`
--
ALTER TABLE `product_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `K_product_group_id` (`id`) USING BTREE;

--
-- Indexes for table `tbl_dosen`
--
ALTER TABLE `tbl_dosen`
  ADD PRIMARY KEY (`nip`),
  ADD KEY `FK58r46u6jo3k8nidxmbdjdm322` (`kode_matkul`);

--
-- Indexes for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD PRIMARY KEY (`nim`),
  ADD KEY `FK3iy5mfvy3f6pb9kahl45979vq` (`kode_matkul`);

--
-- Indexes for table `tbl_matakuliah`
--
ALTER TABLE `tbl_matakuliah`
  ADD PRIMARY KEY (`kode_mk`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mst_permissions`
--
ALTER TABLE `mst_permissions`
  ADD CONSTRAINT `FKdt7nrv7thaaiul04xg3o883k3` FOREIGN KEY (`role_id`) REFERENCES `mst_role` (`id`);

--
-- Constraints for table `mst_user`
--
ALTER TABLE `mst_user`
  ADD CONSTRAINT `FKobfmimbx942u4stre3a84a6av` FOREIGN KEY (`role_id`) REFERENCES `mst_role` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`product_group_id`) REFERENCES `product_group` (`id`),
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`product_sales_id`) REFERENCES `product_sales` (`id`);

--
-- Constraints for table `tbl_dosen`
--
ALTER TABLE `tbl_dosen`
  ADD CONSTRAINT `FK58r46u6jo3k8nidxmbdjdm322` FOREIGN KEY (`kode_matkul`) REFERENCES `tbl_matakuliah` (`kode_mk`);

--
-- Constraints for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD CONSTRAINT `FK3iy5mfvy3f6pb9kahl45979vq` FOREIGN KEY (`kode_matkul`) REFERENCES `tbl_matakuliah` (`kode_mk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
