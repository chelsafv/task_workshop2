/*
Navicat MySQL Data Transfer

Source Server         : MYSQLocal
Source Server Version : 80020
Source Host           : localhost:3306
Source Database       : training2

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2021-02-21 15:07:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cfg_permission`
-- ----------------------------
DROP TABLE IF EXISTS `cfg_permission`;
CREATE TABLE `cfg_permission` (
  `id` varchar(50) NOT NULL,
  `modul` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of cfg_permission
-- ----------------------------
INSERT INTO `cfg_permission` VALUES ('ACT', 'Account Opening', 'http://localhost/acct/opening', 'TEL');
INSERT INTO `cfg_permission` VALUES ('INQ', 'Account Inquiry', 'http://localhost/acct/inquiry', 'CS');
INSERT INTO `cfg_permission` VALUES ('TXN', 'Inhouse Transaction', 'http://localhost/txn/inhouse', 'CUS');
INSERT INTO `cfg_permission` VALUES ('TXN2', 'Remmitance', 'http://localhost/internationalTransfer', 'CUS');
INSERT INTO `cfg_permission` VALUES ('UM', 'User Management', 'http://localhost/usr/management', 'ADM');

-- ----------------------------
-- Table structure for `mst_role`
-- ----------------------------
DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE `mst_role` (
  `id` varchar(50) NOT NULL,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of mst_role
-- ----------------------------
INSERT INTO `mst_role` VALUES ('ADM', 'Admin', 'Administrator');
INSERT INTO `mst_role` VALUES ('CS', 'CustService', 'Customer Service');
INSERT INTO `mst_role` VALUES ('CUS', 'Customer', 'Nasabah Bank');
INSERT INTO `mst_role` VALUES ('TEL', 'Teller', 'Teller Bank');

-- ----------------------------
-- Table structure for `mst_user`
-- ----------------------------
DROP TABLE IF EXISTS `mst_user`;
CREATE TABLE `mst_user` (
  `id` varchar(40) NOT NULL,
  `email` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `role_id` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of mst_user
-- ----------------------------
INSERT INTO `mst_user` VALUES ('1', 'budi@mail.com', 'Budi', 'Yogyakarta', 'ADM');
INSERT INTO `mst_user` VALUES ('2', 'brian@mail.com', 'Brian', 'Yogyakarta', 'CS');
INSERT INTO `mst_user` VALUES ('3', 'elvin@mail.com', 'Elvin', 'Bantul', 'CUS');
INSERT INTO `mst_user` VALUES ('4', 'ulil@mail.com', 'Ulil', 'Sleman', 'TEL');
